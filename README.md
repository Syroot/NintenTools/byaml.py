# byaml
Python package for loading and saving data in the Nintendo BYAML format.

## PyPI
The package can be found on [PyPI](https://pypi.python.org/pypi/byaml).
